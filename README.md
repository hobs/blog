# Blog (django site)

Travel blog for hobs

## Install and serve

Launch tmux before all this.

```bash
git clone git@gitlab.com:hobs/blog && cd blog
python -m virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -e .
python manage.py migrate
python manage.py runserver &
sudo tailscale serve 8000
```

Now you can copy the tailscale URL to your website and [ctrl-b] [ctrl-d] to detach the tmux session and exit the terminal/ssh.


