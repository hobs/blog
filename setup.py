from setuptools import setup, find_packages

setup(
    name='blog',
    version='0.1.0',
    description='Hobs\' personal travel blog rendered with Django 5.0+',
    license='GPL-3.0',
    packages=['blog'],
)
